FROM python:3.7-slim-stretch

RUN apt-get update && apt-get install -y --no-install-recommends gcc libc-dev

COPY ./app ./app

COPY ./main.py ./main.py

EXPOSE 8000

COPY ./requirements.txt ./requirements.txt

COPY ./.env ./.env

COPY ./app.log ./app.log

COPY ./makeCython.py ./makeCython.py

RUN pip install --no-cache-dir -r requirements.txt

RUN python3 makeCython.py build_ext --inplace

CMD ["uvicorn", "main:app", "--host", "0.0.0.0", "--port", "8000", "--log-level", "critical"]