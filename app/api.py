import time

import dotenv,logging,requests
from fastapi import FastAPI, Request, Response, status
from .v3.entity.route_request import RouteRequest
from .v3.controller.route_info_controller import RouteController
from .v3.entity.route_response import ErrorClass,PydanticRouteResponse
from typing import Union
from .config.vrpconfig import VRPConfig
from .v2.controller.route_info_controller import RouteController as RouteControllerV2
from .v2.entity.pydanticModels.pydantic_route_request import RouteRequestSingleVehicle
from .v2.entity.route_request import RouteRequest as RouteRequestV2
from time import perf_counter

dotenv.load_dotenv(dotenv_path='.env')
app = FastAPI(docs_url='/docs',redoc_url='/redoc')


@app.post("/get-multiple-routes",response_model=Union[PydanticRouteResponse,ErrorClass])
async def Multiple_Vehicle_Pickup_And_Delivery_Optimization(routeRequest: RouteRequest, response: Response):
    try:
        startTime = time.perf_counter()

        tripDetail = RouteController().getRoutes(routeRequest)
        if isinstance(tripDetail, ErrorClass):
            if tripDetail.errorMessages[0] == 'Internal Server Error.':
                response.status_code = 500
            else:
                response.status_code = 400
            endTime = time.perf_counter()
            print("program execution time ", endTime-startTime)
            return tripDetail
        endTime = time.perf_counter()
        print("program execution time ", endTime - startTime)
        return tripDetail
    except Exception as e:
        response.status_code = 500
        logging.getLogger('API').error("", exc_info=True)
        return ErrorClass(errorMessages=['Internal Server Error'])


@app.post("/get-routes")
async def SingleVehicle_Pickup_and_Delivery_Optimization(routeRequest: RouteRequestSingleVehicle,response: Response):
    try:
        routeRequestObj = RouteRequestV2(routeRequest.dict())
        tripDetail = RouteControllerV2().getRoutes(routeRequestObj)
        if tripDetail.get('errorMessages'):
            del tripDetail['serverError']
            response.status_code=400
            return tripDetail
        elif tripDetail['serverError']:
            del tripDetail['serverError']
            response.status_code=500
            return tripDetail
        del tripDetail['serverError']
        return tripDetail
    except Exception as e:
        response.status_code=500
        logging.getLogger('API').error("",exc_info=True)
        return {'errorMessages' : "Internal Server Error."}

@app.get('/version')
async def version():
    return {'version': 'v4.0.0'}

@app.get('/health')
async def healthCheck(response: Response):
    try:
        vrpInstance=VRPConfig()
        if(requests.get(f'https://api.openrouteservice.org/v2/directions/driving-car?api_key={vrpInstance.getApikey()}&start=8.681495,49.41461&end=8.687872,49.420318').status_code!=200):
            response.status_code=500
            return {'ServiceCheck': 'Down'}
        return {'ServiceCheck': 'Running'}
    except Exception as e:
        response.status_code=500
        logging.getLogger('API').error('',exc_info=True)
        return {'ServiceCheck': 'Down'}