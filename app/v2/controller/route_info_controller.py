import logging
from ..impl.vrp_impl import VRPImpl


class RouteController:
    def __init__(self):
        self.logger = logging.getLogger('route_info_controller')

    def getRoutes(self, routeRequest):
        try:
            route_info_response = VRPImpl(routeRequest).getRoutes()
            write_list = {}
            write_list['serverError']=route_info_response.internalServerError
            if route_info_response.status != 'FAILURE' and not route_info_response.internalServerError:
                self.logger.info("RouteInfoController.getRoutes() routeInfoResponse =  ")
                trip_detail_dic = vars(route_info_response)
                write_list['id'] = trip_detail_dic['id']
                write_list['tripId'] = trip_detail_dic['tripId']
                write_list['depoId'] = str(trip_detail_dic['depoId'])
                write_list['depoLong'] = trip_detail_dic['depoLong']
                write_list['depoLat'] = trip_detail_dic['depoLat']
                write_list['vehicleCapacity'] = None
                write_list['tripOrders'] = trip_detail_dic['trip']
            else:
                write_list['errorMessages'] = route_info_response.getErrorMessages()
            return write_list
        except Exception as e:
            self.logger.error('',exc_info=True)