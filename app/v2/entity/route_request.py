from typing import List
class RouteRequest:
    def __init__(self, data: dict):
        self._tripId = data["tripId"]
        self._optimizationAPIKey = data["optimizationAPIKey"]
        self._depot = data['depot']
        self._vehicleCapacities = data['vehicleCapacities']
        self._visitLocationList = data["visitLocationList"]
        self._DeliveryAndPickups = []
        self.overlap = False
        deliver,pickup={},{}
        nodes=set()
        for index,location in enumerate(data['visitLocationList']):
            orderIds=location['orderIds'].split(':')
            orderIds.append(location['id'])
            for orderId in orderIds:
                if orderId.endswith('-1'):
                    orderId=orderId[:-2]
                    if(not self.overlap and index+1 not in nodes):
                        nodes.add(index+1)
                    elif(not self.overlap and index+1 in nodes):
                        self.overlap=True
                    if orderId in deliver:
                        self._DeliveryAndPickups.append((index+1,deliver[orderId]))
                        del deliver[orderId]
                    else:
                        pickup[orderId]=index+1
                elif orderId.endswith('-2'):
                    if(not self.overlap and index+1 not in nodes):
                        nodes.add(index+1)
                    elif(not self.overlap and index+1 in nodes):
                        self.overlap=True
                    orderId=orderId[:-2]
                    if orderId in pickup:
                        self._DeliveryAndPickups.append((pickup[orderId],index+1))
                        del pickup[orderId]
                    else:
                        deliver[orderId]=index+1
    def getTripId(self) -> str:
        return self._tripId

    def getOptimizationAPIKey(self) -> str:
        return self._optimizationAPIKey

    def getDepot(self) -> dict:
        return self._depot

    def getVehicleCapacities(self) -> List[int]:
        return self._vehicleCapacities

    def getLocationDetails(self) -> List[dict]:
        return self._visitLocationList

    def getDeliveryAndPickups(self):
        return self._DeliveryAndPickups