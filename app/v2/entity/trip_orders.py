class TripOrders:
    def __init__(self):
        self.id = None
        self.orderId = int
        self.tripId = None
        self.latitude = float
        self.longitude = float
        self.description = None
        self.itemsToDeliver = int
        self.sequenceNo = int
        self.orderIds = str

    def setTripOrder(self, orderId, latitude, longitude, itemsToDeliver, sequenceNo, orderIds):
        self.orderId = orderId
        self.latitude = latitude
        self.longitude = longitude
        self.itemsToDeliver = itemsToDeliver
        self.sequenceNo = sequenceNo
        self.orderIds = orderIds

    def setOrderId(self, param):
        self.orderId = param

    def setLatitude(self, param):
        self.latitude = param

    def setLongitude(self, param):
        self.longitude = param

    def setItemsToDeliver(self, param):
        self.itemsToDeliver = param

    def setOldSequenceNo(self, param):
        self.sequenceNo = param

    def setSequenceNo(self, param):
        self.sequenceNo = param
