from typing import List
from pydantic import BaseModel

class pydanticLocation(BaseModel):
    id: str
    latitude: float
    longitude: float
    orderIds: str
    itemsToDeliver: int
    oldSequenceNo: int

class BaseRequestClass(BaseModel):
    tripId: str
    optimizationAPIKey: str
    depot: pydanticLocation
    visitLocationList: List[pydanticLocation]

class RouteRequestMultipleVehicle(BaseRequestClass):
    vehicleDetails: List[str]

class RouteRequestSingleVehicle(BaseRequestClass):
    vehicleCapacities: List[int]