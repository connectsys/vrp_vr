class TripDetail:
    def __init__(self,routeRequest):
        self.DId = routeRequest.getDepot()['id']
        self.Long = routeRequest.getDepot()['longitude']
        self.Lat = routeRequest.getDepot()['latitude']
        self.Capacity = routeRequest.getVehicleCapacities()
        self.tripOrders = []
        for order in range(0, len(routeRequest.getLocationDetails())):
            self.tripOrders.append(routeRequest.getLocationDetails()[order]['orderIds'])

    def getTripId(self):
        return self.id

    def setTripId(self, trip_id):
        self.VId = trip_id

    def getDepotId(self):
        return self.DId

    def setDepotId(self, depot_id):
        self.DId = depot_id

    def getDepotLong(self):
        return self.Long

    def setDepotLong(self, depot_long):
        self.Long = depot_long

    def getDepotLat(self):
        return self.Lat

    def setDepotLat(self, depot_lat):
        self.Lat = depot_lat

    def getVehicleCapacity(self):
        return self.Capacity

    def setVehicleCapacity(self, vehicle_capacity):
        self.Capacity = vehicle_capacity

    def getTripOrders(self):
        return self.tripOrders

    def setTripOrders(self, trip_orders):
        self.tripOrders = trip_orders

    def getId(self):
        return id

    def setId(self, id):
        self.id = id
