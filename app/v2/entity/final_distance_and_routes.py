from typing import List
class FinalDistanceAndRoute:

    def __init__(self):
        self.totalDistance = int
        self.totalLoad = int
        self.visitOrderLocationIds = []
        self.visitOrderCoordinates = []
        self.tripDetail = []

    def getTotalDistance(self) -> int:
        return self.totalDistance

    def setTotalDistance(self, total_distance: int) -> None:
        self.totalDistance = total_distance

    def getVisitOrderLocationIds(self) -> list:
        return self.visitOrderLocationIds

    def setVisitOrderLocationIds(self, visit_order_location_ids):
        self.visitOrderLocationIds = visit_order_location_ids

    def getVisitOrderCoordinates(self):
        return self.visitOrderCoordinates

    def setVisitOrderCoordinates(self, visit_order_coordinates):
        self.visitOrderCoordinates = visit_order_coordinates

    def getTotalLoad(self) -> int:
        return self.totalLoad

    def setTotalLoad(self, total_load : int):
        self.totalLoad = total_load

    def getTripDetail(self) -> List[dict]:
        return self.tripDetail

    def setTripDetail(self, trip_detail: dict):
        self.tripDetail.append(trip_detail)

    def toDef(self):
        return f"FinalDistanceAndRoute: totalDistance= {self.totalDistance}, totalLoad= {self.totalLoad}" \
               f"visitOrderLocationIds= '{self.visitOrderLocationIds}' " \
               f"visitOrderCoordinates= {self.visitOrderCoordinates}.\n"
