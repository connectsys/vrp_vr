import logging


class RouteResponse:
    def __init__(self):
        self.id = None
        self.depoId = int
        self.depoLong = float
        self.depoLat = float
        self.vehicleCapacity = int
        self.trip=[]
        self.status = str
        self.errorMessages = []
        self.tripId=str
        self.internalServerError=False

    def setId(self, id):
        self.id = id

    def setTripId(self, tripId):
        self.tripId = tripId

    def setDepoId(self, depotId):
        self.depoId = depotId

    def setDepoLong(self, depotLong):
        self.depoLong = depotLong

    def setDepoLat(self, depotLat):
        self.depoLat = depotLat

    def setVehicleCapacity(self, vehicleCap):
        self.vehicleCapacity = vehicleCap

    def setTripOrders(self, orders):
        self.trip = orders

    def setStatus(self, status):
        self.status = status

    def ErrorMessages(self, message):
        self.errorMessages.append(message)

    def getId(self):
        return self.id

    def getTripId(self):
        return self.tripId

    def getDepoId(self):
        return self.depoId

    def getDepoLong(self):
        return self.depoLong

    def getDepoLat(self):
        return self.depoLat

    def getVehicleCapacity(self):
        return self.vehicleCapacity

    def getTripOrders(self):
        return self.trip

    def getStatus(self):
        return self.status

    def getErrorMessages(self):
        return self.errorMessages
