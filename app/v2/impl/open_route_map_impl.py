import requests as requests
from .abstact_map_impl import AbstractMapSvcImpl
from ...config.vrpconfig import VRPConfig
import logging


class OpenRouteImpl:
    def __init__(self,routeRequest):
        self.configuration = VRPConfig()
        self.amsi = AbstractMapSvcImpl()
        self.RReq = routeRequest
        self.logger = logging.getLogger('OpenRouteImpl')

    def getRoutes(self, routeRequest):
        input_validation = self.amsi.validateInputs(routeRequest)
        if input_validation.status != "FAILURE":
            self.logger.info("Getting distance matrix")
            long_lat = self.getLongLat()
            self.logger.info("Lat_long")
            distance_matrix = self.getDistanceMatrix(long_lat)
            if distance_matrix['status']=='FAILURE':
                input_validation.status='FAILURE'
                input_validation.ErrorMessages('Internal Server Error.')
                input_validation.internalServerError=True
                return input_validation
            if distance_matrix['status']=='INVALID':
                input_validation.status='FAILURE'
                input_validation.ErrorMessages("OR-tools can't arrive at a Solution with Given Coordinates.")
                return input_validation
            return distance_matrix['data']
        else:
            self.logger.error("Failed to get distance matrix")
            return input_validation

    def getLongLat(self):
        long_lat = []
        long_lat.insert(0, [self.RReq.getDepot()['longitude'], self.RReq.getDepot()['latitude']])
        self.logger.info(self.RReq.getVehicleCapacities())
        for i in range(0, len(self.RReq.getLocationDetails())):
            long_lat.insert(i+1, [self.RReq.getLocationDetails()[i]['longitude'],
                            self.RReq.getLocationDetails()[i]['latitude']])

        return long_lat

    def getDistanceMatrix(self, long_lat):
        header = {
            'Authorization': self.RReq.getOptimizationAPIKey()
        }
        body = {
            'locations': long_lat,
            'metrics': ["distance"],
            'units': self.configuration.getUnits()
        }
        map_api = self.configuration.getMapURL()
        optimization_api=self.RReq.getOptimizationAPIKey()
        self.logger.info(body)
        DistanceMatrix={'data': [], 'status': "SUCCESS"}
        try:
            response = requests.post(url=map_api, headers=header, json=body)
        except requests.exceptions.RequestException:
            self.logger.error('No response from OpenRouteMapAPI.')
            DistanceMatrix['status']="FAILURE"
            return DistanceMatrix
        if(response.status_code!=200):
            header['Authorization']=self.configuration.getApikey()
            response = requests.post(url=map_api, headers=header, json=body)
            if(response.status_code!=200):
                self.logger.error('No response from OpenRouteMapAPI.')
                DistanceMatrix['status']="FAILURE"
                return DistanceMatrix
        self.logger.info(f"{response.text}")
        DistanceMatrix['data'] = response.json()['distances']
        self.logger.info(DistanceMatrix)
        for distances in DistanceMatrix['data']:
            if None in distances:
                DistanceMatrix['status']='INVALID'                                  #Used for when the given coordinates are invalid.
                break
        return DistanceMatrix