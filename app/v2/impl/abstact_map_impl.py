from ..entity.route_response import RouteResponse
import logging


class AbstractMapSvcImpl:
    logger = logging.getLogger('abstractmapsvcimpl')
    logger.info("Validating Inputs")

    def validateInputs(self,routeRequest):
        routeInfoResponse = RouteResponse()
        routeInfoRequest = routeRequest
        routeInfoResponse.setStatus("SUCCESS")
        if None is routeInfoRequest.getTripId() or len(str(routeInfoRequest.getTripId())) == 0:
            routeInfoResponse.setStatus("FAILURE")
            routeInfoResponse.ErrorMessages("TripId cannot be null or blank.")

        if len(routeInfoRequest.getDepot()["id"]) == 0:
            routeInfoResponse.setStatus("FAILURE")
            routeInfoResponse.ErrorMessages("Depot location details cannot be null.")

        if None is routeInfoRequest.getVehicleCapacities() or len(routeInfoRequest.getVehicleCapacities()) == 0:
            routeInfoResponse.setStatus("FAILURE")
            routeInfoResponse.ErrorMessages("Vehicle capacities cannot be null or empty. ")

        if len(routeInfoRequest.getLocationDetails()) == 0:
            routeInfoResponse.setStatus("FAILURE")
            routeInfoResponse.ErrorMessages("At least one delivery location needs to be present. ")

        totalCapacity = 0
        if None is not routeInfoRequest.getVehicleCapacities() and len(routeInfoRequest.getVehicleCapacities()) != 0:
            totalCapacity=routeInfoRequest.getVehicleCapacities()[0]

        totalLoad = 0
        if len(routeInfoRequest.getLocationDetails()) != 0:
            for i in range(0, len(routeInfoRequest.getLocationDetails())):
                totalLoad += routeInfoRequest.getLocationDetails()[i]['itemsToDeliver']

        if totalCapacity < totalLoad:
            routeInfoResponse.setStatus("FAILURE")
            routeInfoResponse.ErrorMessages("Total load that needs to be delivered cannot exceed capacity of vehicle(s)"
                                            ". ")

        return routeInfoResponse
