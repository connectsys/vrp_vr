from ortools.constraint_solver import routing_enums_pb2
from ortools.constraint_solver import pywrapcp
from ortools.constraint_solver.pywrapcp import RoutingIndexManager, RoutingModel

from ...config.vrpconfig import VRPConfig
from ..entity.final_distance_and_routes import FinalDistanceAndRoute
from ..entity.route_response import RouteResponse
from ..entity.trip import TripDetail
from .open_route_map_impl import OpenRouteImpl


class VRPImpl:
    def __init__(self,routeRequest):
        self.configurations = VRPConfig()
        self.RIRes = RouteResponse()
        self.RIReq = routeRequest
        self.url = self.configurations.getMapURL()
        self.ORM = OpenRouteImpl(routeRequest)

    def getRoutes(self):
        distance_matrix = self.ORM.getRoutes(self.RIReq)
        if type(distance_matrix) == list:
            routeInfoResponse = self.calculateRoutes(distance_matrix)
            return routeInfoResponse
        return distance_matrix

    def printSolution(self, distanceMatrix, noOfVehicles, itemsToDropPerLocation, depot, routing, manager,
                      solution):

        route_distance = 0
        for vehicle_id in range(noOfVehicles):
            totalLoad = 0
            routeLoad = 0
            totalDistance = 0
            index = routing.Start(vehicle_id)
            plan_output = 'Route for vehicle {}:\n'.format(vehicle_id)
            while not routing.IsEnd(index):
                nodeIndex = manager.IndexToNode(index)
                routeLoad += itemsToDropPerLocation[(int(nodeIndex))]
                plan_output += ' {} -> '.format(manager.IndexToNode(index))
                previous_index = index
                index = solution.Value(routing.NextVar(index))
                from_node = manager.IndexToNode(previous_index)
                to_node = manager.IndexToNode(index)
                distance = distanceMatrix[from_node][to_node]
                route_distance += distance
            plan_output += '{}\n'.format(manager.IndexToNode(index))
            plan_output += 'Distance of the route: {}km\n'.format(route_distance)
            totalDistance += route_distance
            totalLoad += routeLoad
            route_distance += route_distance

    def getFinalDistanceAndRoute(self, distanceMatrix, numberOfVehicles, depot,
                                 routing, manager, solution, routeInfoRequest):
        _trip = TripDetail(self.RIReq)
        tripOrderDetails = []
        depotAndLocationsList = [self.RIReq.getDepot()]
        _trip.setDepotId(self.RIReq.getDepot()["id"])
        _trip.setDepotLat(self.RIReq.getDepot()["latitude"])
        _trip.setDepotLong(self.RIReq.getDepot()["longitude"])
        _trip.setTripId(self.RIReq.getTripId())
        _trip.setVehicleCapacity(self.RIReq.getVehicleCapacities()[0])
        loc_list = self.RIReq.getLocationDetails()
        for i in range(0, len(self.RIReq.getLocationDetails())):
            depotAndLocationsList.append(loc_list[i])

        finalDistanceAndRoute = FinalDistanceAndRoute()
        totalDistance = 0
        totalLoad = 0
        for i in range(0, numberOfVehicles):
            loc=[]
            index = routing.Start(i)
            routeDistance = 0
            i = 1
            while not routing.IsEnd(index):
                nodeIndex = manager.IndexToNode(index)
                previous_index = index
                if nodeIndex != 0:
                    tripOrders = {'id': None, 'orderId': str(depotAndLocationsList[nodeIndex]['id']), 'latitude': depotAndLocationsList[nodeIndex]['latitude'],
                                  'longitude': depotAndLocationsList[nodeIndex]["longitude"], 'description': None,
                                  'itemsToDeliver': depotAndLocationsList[nodeIndex]["itemsToDeliver"],
                                  'sequenceNo': i, 'orderIds': depotAndLocationsList[nodeIndex]["orderIds"]
                                  }
                    i += 1
                    loc.append(tripOrders)
                index = solution.Value(routing.NextVar(index))
                from_node = manager.IndexToNode(previous_index)
                to_node = manager.IndexToNode(index)
                distance = distanceMatrix[from_node][to_node]
                routeDistance += distance
            totalDistance += routeDistance
            finalDistanceAndRoute.setTripDetail(loc)

            _trip.setTripOrders(tripOrderDetails)
            finalDistanceAndRoute.setTotalDistance(routeDistance)
            finalDistanceAndRoute.setTotalLoad(totalLoad)
        return finalDistanceAndRoute

    def calculateRoutes(self,distanceMatrix):
        vehicleCapacities = self.RIReq.getVehicleCapacities()
        numberOfVehicles = len(vehicleCapacities)

        depot = 0
        manager = RoutingIndexManager(len(distanceMatrix), numberOfVehicles, depot)
        routingModel = RoutingModel(manager)

        def distance_callback(from_index, to_index):
            """Returns the distance between the two nodes."""
            # Convert from routing variable Index to distance matrix NodeIndex.
            from_node = manager.IndexToNode(from_index)
            to_node = manager.IndexToNode(to_index)
            return distanceMatrix[from_node][to_node]

        transit_callback_index = routingModel.RegisterTransitCallback(distance_callback)

        # Define cost of each arc.
        routingModel.SetArcCostEvaluatorOfAllVehicles(transit_callback_index)
        routingModel.AddDimension(
            transit_callback_index,
            0,
            100000000000,
            True,
            'Distance'
        )
        distanceDimension=routingModel.GetDimensionOrDie('Distance')
        for pickup,deliver in self.RIReq.getDeliveryAndPickups():
            pickupIndex=manager.NodeToIndex(pickup)
            deliverIndex=manager.NodeToIndex(deliver)
            if(not self.RIReq.overlap):
                routingModel.AddPickupAndDelivery(pickupIndex, deliverIndex)
            # constraintActive = routingModel.ActiveVar(manager.NodeToIndex(pickup)) * routingModel.ActiveVar(manager.NodeToIndex(deliver))
            routingModel.solver().Add(
                distanceDimension.CumulVar(pickupIndex) <=
                distanceDimension.CumulVar(deliverIndex)
            )
        timeLimit = self.configurations.getTimeLimit()

        search_parameters = pywrapcp.DefaultRoutingSearchParameters()
        search_parameters.time_limit.FromSeconds(timeLimit)
        search_parameters.first_solution_strategy = (
            routing_enums_pb2.FirstSolutionStrategy.PATH_CHEAPEST_ARC)  # pylint: disable=no-member
        search_parameters.local_search_metaheuristic = (
            routing_enums_pb2.LocalSearchMetaheuristic.GUIDED_LOCAL_SEARCH)

        solution = routingModel.SolveWithParameters(search_parameters)

        if solution:
            # self.printSolution(distanceMatrix, numberOfVehicles, itemsToDropPerLocation, depot, routingModel, manager,
            #                    solution)
            finalDistanceAndRoute = self.getFinalDistanceAndRoute(distanceMatrix, numberOfVehicles,
                                                                depot, routingModel, manager,
                                                                  solution, self.RIReq)
            self.RIRes.setDepoId(self.RIReq.getDepot()['id'])
            self.RIRes.setDepoLat(self.RIReq.getDepot()['latitude'])
            self.RIRes.setVehicleCapacity(self.RIReq.getVehicleCapacities())
            self.RIRes.setDepoLong(self.RIReq.getDepot()['longitude'])
            self.RIRes.setStatus("SUCCESS")
            self.RIRes.setTripId(self.RIReq.getTripId())
            self.RIRes.setTripOrders(finalDistanceAndRoute.getTripDetail()[0])    #[0] is added so we get only the first tripOrder which is what we get in single Vehicle
                                                                                  #In case of Multi Vehicle Changes This.
        else:
            self.RIRes.setStatus("FAILURE")
            self.RIRes.ErrorMessages("OR engine cannot arrive at a solution with given parameters. Please check all the"
                                     " inputs and try again")

        return self.RIRes
