import logging
from typing import List,Optional
from pydantic import BaseModel


class Order(BaseModel):
    orderId: str
    weight: float
    volume: float
    mode: str


class Trip(BaseModel):
    latitude: float
    longitude: float
    locationWeight: float
    locationVolume: float
    sequenceNo: int
    oldSequenceNo: int
    orderDetailsList: List[Order]


class TripDetail(BaseModel):
    shiftId: int
    vehicleId: int
    driverId: int
    maxTripWeight: float
    maxTripVolume: float
    maxNumberOfOrders: int
    travelDistance: float
    travelTime: float
    locationDetailsList: List[Trip]


class PydanticRouteResponse(BaseModel):
    depotIndex: int
    depotLat: float
    depotLng: float
    routeDetailsList: List[TripDetail]
    rejectedOrderIdsList: List[str]

    def __init__(self, **kwargs):
        super().__init__(**kwargs)

    def setDepoId(self, depotId):
        self.depoId = depotId

    def setdepotLng(self, depotLng):
        self.depotLng = depotLng

    def setDepoLat(self, depotLat):
        self.depoLat = depotLat

    def setrouteDetailsList(self, orders):
        self.routeDetailsList = orders

    def setDroppedOrders(self, droppedOrders):
        self.rejectedOrderIdsList = droppedOrders
        
    def setStatus(self, status):
        self._status = status

    def ErrorMessages(self, message):
        self._errorMessages.append(message)

    def getDepoIndex(self):
        return self.depotIndex

    def getdepotLng(self):
        return self.depotLng

    def getDepoLat(self):
        return self.depoLat

    def getrouteDetailsList(self):
        return self.routeDetailsList

    def getStatus(self):
        return self._status

    def getErrorMessages(self):
        return self._errorMessages


class ErrorClass(BaseModel):
    errorMessages: List[str]
