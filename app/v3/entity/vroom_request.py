from pydantic import BaseModel
from typing import List, Optional


class VroomJob(BaseModel):
    id: int
    description: Optional[str]
    location: Optional[List[int]]
    location_index: int
    setup: Optional[int] = 0
    service: Optional[int] = 0
    delivery: Optional[List[int]]
    pickup: Optional[List[int]]
    skills: Optional[List[int]]
    priority: Optional[int] = 0
    time_windows: Optional[List[List[int]]]


class VroomShipmentStep(BaseModel):
    id: int
    description: Optional[str]  
    location: Optional[List[int]]  
    location_index: int
    setup: Optional[int] = 0
    service: Optional[int] = 0
    time_windows: Optional[List[List[int]]]


class VroomShipment(BaseModel):
    pickup: Optional[VroomShipmentStep]  
    delivery: Optional[VroomShipmentStep]  
    amount: Optional[List[int]]  
    skills: Optional[List[int]]  
    priority: Optional[int] = 0


class VroomBreak(BaseModel):
    id: int
    time_windows: Optional[List[List[int]]]
    service: Optional[int] = 0
    description: Optional[str]  


class VroomVehicleStep(BaseModel):
    type: str
    id: Optional[int]  
    service_at: Optional[int]  
    service_after: Optional[int]  
    service_before: Optional[int]  


class VroomVehicle(BaseModel):
    id: int
    profile: Optional[str] = "car"
    description: Optional[str]  
    start: Optional[List[int]]
    start_index: Optional[int]  
    end: Optional[List[int]]
    end_index: Optional[int]  
    capacity: Optional[List[int]]  
    skills: Optional[List[int]]  
    time_window: Optional[List[List[int]]]
    breaks: Optional[List[VroomBreak]]  
    speed_factor: Optional[int] = 1
    max_tasks: Optional[int]  
    steps: Optional[List[VroomVehicleStep]]  


class VroomDurationCost(BaseModel):
    durations: List[List[int]]
    costs: Optional[List[List[int]]]


class VroomMatrices(BaseModel):
    car: Optional[VroomDurationCost]


class VroomRequest(BaseModel):
    jobs: Optional[List[VroomJob]]
    shipments: Optional[List[VroomShipment]]
    vehicles: List[VroomVehicle]
    matrices: VroomMatrices
