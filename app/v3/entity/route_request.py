"""
Checks that has to be done:
1. At least One Vehicle Detail should be present in VehicleDetails List
2. VehicleShiftTime<=startTime(A.K.A min(vehicleShiftTime))<=endTime(A.K.A max(vehicleShiftTIme))
3. There Should be no locations in locationDetails Which are not part of orders.  //Just a Saftey Check if not Followed the the request would be considered as a TSP Problem.
Serious Checks(These Checks needs to be Done Necessarily or whole machine Crashes Cause of Lack of memory):
1. If there is weights>0 in any order then vehicleCapacity of all vehicles Should not be zero. At least One should have vehicleCapcity>0.
2. if there is volume>0 in any order then at least one Vehicle Should have totalVomax0.
"""

from typing import List, Optional, Union
from pydantic import BaseModel
from ..impl.abstact_map_impl import AbstractMapSvcImpl
from datetime import datetime
from pydantic.error_wrappers import ValidationError
import logging
import os


class TimeWindow(BaseModel):
    # Both Should be Optional Cause of Cases Where there's only a startTime or endTime
    startTime: Optional[datetime] = None
    # In case of startTime=Null set startTime=tripStartTime in Case of endTime=Null set endTime=999999999999999
    endTime: Optional[datetime] = None


class Location(BaseModel):
    id: Optional[str] = 'Something'
    latitude: float
    longitude: float


class Order(BaseModel):
    orderId: str
    weight: Optional[float] = 0
    volume: Optional[float] = 0
    order: Optional[int] = 0
    pickupIndex: Optional[int] = None
    deliveryIndex: Optional[int] = None
    pickupTimeWindow: Optional[TimeWindow] = None
    deliveryTimeWindow: Optional[TimeWindow] = None
    serviceTimeDelivery: Optional[int] = 600
    serviceTimePickup: Optional[int] = 600


class Vehicle(BaseModel):
    shiftId: int
    vehicleId: int
    driverId: int
    weightCapacity: Optional[float] = 999999
    volumeCapacity: Optional[float] = 999999
    orderCapacity: Optional[float] = 999999
    range: Optional[float] = 999999
    shiftTimeWindow: Optional[TimeWindow] = None


class RouteRequest(BaseModel):
    optimizationAPIKey: Optional[str] = 'string'
    units: Optional[str] = 'm'
    geoCode: Optional[str] = 'openRouteMapURL'
    solver: Optional[str] = 'distance'
    weightConstraint: Optional[bool] = True
    volumeConstraint: Optional[bool] = True
    timeWindowsConstraint: Optional[bool] = True
    orderConstraint: Optional[bool] = True
    penaltyConstraint: Optional[bool] = True
    depotIndex: Optional[int] = 0
    vehicleDetailsList: List[Vehicle]
    locationDetailsList: List[Location]
    orderDetailsList: List[Order]

    def inputConversion(self, n):
        return int(1000 * (float(n)))

    def __init__(self, **kwargs):

        try:
            super().__init__(**kwargs)
            self.vehicleDetailsList.sort(key=lambda x: x.range)
            for i in self.vehicleDetailsList:
                i.weightCapacity = self.inputConversion(i.weightCapacity)
                i.volumeCapacity = self.inputConversion(i.volumeCapacity)
                if i.orderCapacity == 0 or i.orderCapacity == 999999:
                    i.orderCapacity = len(self.orderDetailsList)
                i.range = int(i.range)
            for i in self.orderDetailsList:
                i.weight = self.inputConversion(i.weight)
                i.volume = self.inputConversion(i.volume)
            object.__setattr__(self, 'errorMessages', [])
            object.__setattr__(self, 'timeWindowSlack', float(os.getenv('timeWindowSlack')))
            object.__setattr__(self, 'numLocations', len(self.getLocationDetails()))
            object.__setattr__(self, '_numVehicles', len(self.getVehicleDetails()))
            object.__setattr__(self, '_rangesVehicles', [i.range for i in self.getVehicleDetails()])
            object.__setattr__(self, 'droppedOrders', [])
            startTime = self.getVehicleDetails()[0].shiftTimeWindow.startTime
            if startTime is not None:
                for i in self.getVehicleDetails():
                    if i.shiftTimeWindow and i.shiftTimeWindow.startTime:
                        startTime = min(startTime, i.shiftTimeWindow.startTime)
                for i in self.getOrders():
                    if i.pickupTimeWindow and i.pickupTimeWindow.startTime:
                        startTime = min(startTime, i.pickupTimeWindow.startTime)
                    if i.deliveryTimeWindow and i.deliveryTimeWindow.startTime:
                        startTime = min(startTime, i.deliveryTimeWindow.startTime)

            object.__setattr__(self, 'startTime', startTime)
            assert isinstance(self.startTime, datetime)
            if self.startTime is not None:
                object.__setattr__(self, '_driverShifts',
                                   [
                                       [
                                           int((i.shiftTimeWindow.startTime - self.startTime).total_seconds()) if (
                                                   i is not None and i.shiftTimeWindow.startTime is not None) else 0.0,
                                           int((i.shiftTimeWindow.endTime - self.startTime).total_seconds()) if (
                                                   i is not None and i.shiftTimeWindow.endTime is not None) else 0.0
                                       ] for i in self.getVehicleDetails()
                                   ]
                                   )
                t = TimeWindow()
                t.startTime = 0.0
                t.endTime = 99999999.0
                for order in self.orderDetailsList:
                    if order.pickupTimeWindow:
                        if order.pickupTimeWindow.startTime and order.pickupTimeWindow.endTime:
                            order.pickupTimeWindow.startTime = (order.pickupTimeWindow.startTime -
                                                                self.startTime).total_seconds()
                            order.pickupTimeWindow.endTime = (order.pickupTimeWindow.endTime
                                                              - self.startTime).total_seconds()
                        elif order.pickupTimeWindow.startTime:
                            order.pickupTimeWindow.startTime = (
                                    order.pickupTimeWindow.startTime - self.startTime).total_seconds()
                            order.pickupTimeWindow.endTime = order.pickupTimeWindow.startTime + self.timeWindowSlack
                        elif order.pickupTimeWindow.endTime:
                            order.pickupTimeWindow.endTime = (
                                    order.pickupTimeWindow.endTime - self.startTime).total_seconds()
                            order.pickupTimeWindow.startTime = order.pickupTimeWindow.endTime - self.timeWindowSlack
                        else:
                            order.pickupTimeWindow = t
                    else:
                        order.pickupTimeWindow = t
                    if order.deliveryTimeWindow:
                        if order.deliveryTimeWindow.startTime and order.deliveryTimeWindow.endTime:
                            order.deliveryTimeWindow.startTime = (
                                    order.deliveryTimeWindow.startTime - self.startTime).total_seconds()
                            order.deliveryTimeWindow.endTime = (
                                    order.deliveryTimeWindow.endTime - self.startTime).total_seconds()
                        elif order.deliveryTimeWindow.startTime:
                            order.deliveryTimeWindow.startTime = (
                                    order.deliveryTimeWindow.startTime - self.startTime).total_seconds()
                            order.deliveryTimeWindow.endTime = order.deliveryTimeWindow.startTime + self.timeWindowSlack
                        elif order.deliveryTimeWindow.endTime:
                            order.deliveryTimeWindow.endTime = (
                                    order.deliveryTimeWindow.endTime - self.startTime).total_seconds()
                            order.deliveryTimeWindow.startTime = order.deliveryTimeWindow.endTime - self.timeWindowSlack
                        else:
                            order.deliveryTimeWindow = t
                    else:
                        order.deliveryTimeWindow = t

            else:
                object.__setattr__(self, '_driverShifts', None)

            if len(self.getLocationDetails()) == 0:
                self.errorMessages.append(
                    "At least one delivery location needs to be present. ")
            AbstractMapSvcImpl.validateInputs(self, self.errorMessages)
        except ValidationError as e:
            raise e
        except:
            logging.error('', exc_info=True)
            return

    def getGeoCode(self):
        return self.geoCode

    def getUnits(self):
        return self.units

    def getDriverShifts(self):
        return self._driverShifts

    def getOrders(self) -> List[Order]:
        return self.orderDetailsList

    def getNumVehicles(self) -> int:
        return self._numVehicles

    def getOptimizationAPIKey(self) -> str:
        return self.optimizationAPIKey

    def getVehicleDetails(self) -> List[Vehicle]:
        return self.vehicleDetailsList

    def getLocationDetails(self) -> List[Location]:
        return self.locationDetailsList

    def getDepot(self) -> Location:
        return self.locationDetailsList[self.depotIndex]

    def getDepotIndex(self) -> int:
        return self.depotIndex
