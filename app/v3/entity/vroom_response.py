from pydantic import BaseModel
from typing import List, Optional
from pydantic.error_wrappers import ValidationError
from .route_response import PydanticRouteResponse, Trip, Order, TripDetail
from .route_request import Vehicle


class VroomTask(BaseModel):
    id: int
    type: str
    description: Optional[str]
    location: Optional[List[float]]
    location_index: Optional[str]


class VroomViolation(BaseModel):
    cause: Optional[str]
    duration: Optional[int]


class VroomStep(BaseModel):
    type: Optional[str]
    arrival: Optional[int]
    duration: Optional[int]
    setup: Optional[int]
    service: Optional[int]
    waiting_time: Optional[int]
    violations: Optional[List[VroomViolation]]
    description: Optional[str]
    location: Optional[List[float]]
    location_index: Optional[int]
    id: Optional[int]
    load: Optional[List[int]]
    distance: Optional[int]


class VroomRoute(BaseModel):
    vehicle: int
    steps: Optional[List[VroomStep]]
    cost: Optional[int]
    setup: Optional[int]
    service: Optional[int]
    duration: Optional[int]
    waiting_time: Optional[int]
    priority: Optional[int]
    violations: Optional[List[VroomViolation]]
    delivery: Optional[List[int]]
    pickup: Optional[List[int]]
    description: Optional[str]
    distance: Optional[int]


class VroomSummary(BaseModel):
    cost: Optional[int]
    routes: Optional[int]
    unassigned: Optional[int]
    setup: Optional[int]
    service: Optional[int]
    duration: Optional[int]
    waiting_time: Optional[int]
    priority: Optional[int]
    violations: Optional[List[VroomViolation]]
    delivery: Optional[List[int]]
    pickup: Optional[List[int]]
    distance: Optional[int]


class VroomResponse(BaseModel):
    code: int
    error: Optional[str]
    summary: Optional[VroomSummary]
    unassigned: Optional[List[VroomTask]]
    routes: Optional[List[VroomRoute]]

    def __init__(self, **kwargs):

        try:
            super().__init__(**kwargs)

        except ValidationError as e:
            raise e
        except:
            logging.error('', exc_info=True)
            return

    def ConvRRes(self, vehIdDict, locationList):
        route_plan = ""
        routeDetailsList = []
        for route in self.routes:
            veh = vehIdDict[route.vehicle]
            assert isinstance(veh, Vehicle)
            shiftId = veh.shiftId
            vehicleId = veh.vehicleId
            driverId = veh.driverId
            route_plan += f"shiftId: {shiftId}\nvehicleId: {vehicleId}\ndriverId: {driverId}\n"
            travelDistance = route.cost
            route_plan += f"distance: {travelDistance*100//veh.range}%\n"
            travelTime = route.duration
            if veh.shiftTimeWindow and veh.shiftTimeWindow.startTime and veh.shiftTimeWindow.endTime:
                route_plan += f"duration: {travelTime*100//(veh.shiftTimeWindow.endTime-veh.shiftTimeWindow.startTime).total_seconds()}%\n"
            count = 0
            prevWeight = 0
            prevVolume = 0
            prevLocId = 0
            orderList = []
            locationWeight = 0
            locationVolume = 0
            maxTripWeight = 0
            maxTripVolume = 0
            tripList = []
            for step in route.steps:
                if step.type == "start":
                    prevLocId = step.location_index
                    prevWeight, prevVolume = step.load
                    route_plan += f"{prevLocId} -> "
                elif step.type == "end":
                    latitude = locationList[step.location_index].latitude
                    longitude = locationList[step.location_index].longitude
                    oldSeqNumber = step.location_index
                    count += 1
                    tripList.append(Trip(latitude=latitude, longitude=longitude, locationWeight=locationWeight/1000,
                                         locationVolume=locationVolume/1000, sequenceNo=count, oldSequenceNo=oldSeqNumber,
                                         orderDetailsList=orderList))
                    locationVolume = 0
                    locationWeight = 0
                    orderList = []
                    prevLocId = step.location_index
                    route_plan += f"{prevLocId}\n"
                else:
                    if len(orderList) > 0 and prevLocId != step.location_index:
                        latitude = locationList[step.location_index].latitude
                        longitude = locationList[step.location_index].longitude
                        oldSeqNumber = step.location_index
                        count += 1
                        tripList.append(Trip(latitude=latitude, longitude=longitude, locationWeight=locationWeight/1000,
                                             locationVolume=locationVolume/1000, sequenceNo=count, oldSequenceNo=oldSeqNumber,
                                             orderDetailsList=orderList))
                        locationVolume = 0
                        locationWeight = 0
                        orderList = []
                    prevLocId = step.location_index
                    route_plan += f"{prevLocId} -> "
                    orderId = step.id
                    weight, volume = step.load
                    if weight > maxTripVolume:
                        maxTripVolume = weight
                    if volume > maxTripWeight:
                        maxTripVolume = volume
                    weight -= prevWeight
                    volume -= prevVolume
                    prevWeight, prevVolume = step.load
                    locationWeight += weight
                    locationVolume += volume
                    mode = "pickup"
                    if weight < 0:
                        weight *= -1
                        volume *= -1
                        mode = "delivery"
                    order = Order(orderId=orderId, weight=weight/1000, volume=volume/1000, mode=mode)
                    orderList.append(order)
            routeDetailsList.append(TripDetail(shiftId=shiftId, vehicleId=vehicleId, driverId=driverId,
                                               maxTripWeight=maxTripWeight/1000, maxTripVolume=maxTripVolume/1000,
                                               maxNumberOfOrders=count,
                                               travelDistance=travelDistance, travelTime=travelTime,
                                               locationDetailsList=tripList))
            route_plan += f"weight: {maxTripWeight*100//veh.weightCapacity}%\n"
            route_plan += f"volume: {maxTripVolume*100//veh.volumeCapacity}%\n"
            route_plan += f"orders: {count*100//veh.orderCapacity}%\n\n"
        depotLat = locationList[0].latitude
        depotLng = locationList[0].longitude
        print(route_plan)
        rejectedOrderIdsList = []
        if len(self.unassigned) > 0:
            rejectedOrderIdsList = [task.id for task in self.unassigned]
        return PydanticRouteResponse(depotIndex=0, depotLat=depotLat, depotLng=depotLng,
                                     routeDetailsList=routeDetailsList,
                                     rejectedOrderIdsList=rejectedOrderIdsList)
