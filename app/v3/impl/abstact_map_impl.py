from ..entity.route_response import ErrorClass
import logging


class AbstractMapSvcImpl:
    logger = logging.getLogger('Abstract map svc impl')
    logger.info("Validating Inputs")

    @staticmethod
    def validateInputs(routeRequest, ErrorMessages):
        routeInfoRequest = routeRequest
        if len(routeInfoRequest.getLocationDetails())!=0 and len(routeInfoRequest.getLocationDetails()[0].id) == 0:
            ErrorMessages.append("Depot location details cannot be null.")

        if routeInfoRequest.getNumVehicles() == 0:
            ErrorMessages.append("Vehicle Details cannot be null or empty.")

        return ErrorClass(errorMessages=ErrorMessages) if ErrorMessages else None
