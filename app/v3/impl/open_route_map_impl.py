import json
import requests as requests
from ...config.vrpconfig import VRPConfig
from ..entity.route_response import ErrorClass
import logging
import time
from time import perf_counter


class OpenRouteImpl:
    def __init__(self, routeRequest):
        self.configuration = VRPConfig(routeRequest.getGeoCode(), routeRequest.getUnits())
        self.timeLimit = self.configuration.getTimeLimit()
        self.RReq = routeRequest
        self.logger = logging.getLogger('OpenRouteImpl')

    def getRoutes(self):
        self.logger.info("Getting distance matrix")
        long_lat = self.getLongLat()
        self.logger.info("Lat_long")
        distance_matrix = self.getTimeAndDistanceMatrix(long_lat)
        if distance_matrix['status'] == 'FAILURE':
            return ErrorClass(errorMessages=['Internal Server Error.'])

        if distance_matrix['status'] == 'INVALID':
            return ErrorClass(errorMessages=["OR-tools can't arrive at a Solution with Given Coordinates."])
        return distance_matrix['data']

    def getLongLat(self):
        self.logger.info(self.RReq.getVehicleDetails())

        long_lat = [(location.longitude, location.latitude)
                    for location in self.RReq.getLocationDetails()]

        return long_lat

    def getTimeLimit(self):
        return self.timeLimit

    def getTimeAndDistanceMatrix(self, long_lat):
        distanceMatrix = []
        timeMatrix = []
        n = len(long_lat)
        print(n)
        print(long_lat)
        depth = 3500//n
        start = 0
        end = depth - 1
        reqCounter = 0
        count = 0
        startTime = 0
        while end < n - 1:
            origin = list(range(start, end + 1))
            if startTime == 0:
                startTime = perf_counter()
            response = self.sendRequest(long_lat, origin)
            if not response:
                print("Error in ORM")
                return {'status': "INVALID"}
            reqCounter += 1
            for i in response['distances']:
                distanceMatrix.append(i)
            for i in response['durations']:
                timeMatrix.append(i)
            start = end + 1
            end += depth
            if reqCounter == 39:
                endTime = perf_counter()
                gap = endTime - startTime
                if gap < 60:
                    time.sleep(60)
                startTime = 0
                reqCounter = 0

        else:
            if reqCounter == 39:
                endTime = perf_counter()
                gap = endTime - startTime
                if gap < 60:
                    time.sleep(60 - gap)
            end = n - 1
            origin = list(range(start, end+1))
            response = self.sendRequest(long_lat, origin)

            if not response:
                print("Error in ORM")
                return {'status': "INVALID"}
            for i in response['distances']:
                distanceMatrix.append(i)
            for i in response['durations']:
                timeMatrix.append(i)
        timeAndDistanceMatrix = {'data': [distanceMatrix,
                                          timeMatrix], 'status': "SUCCESS"}
        # put 0.1, 5 and 30 in .env
        self.logger.info(timeAndDistanceMatrix)
        if not timeAndDistanceMatrix['data'][0][0]:
            timeAndDistanceMatrix['status'] = 'INVALID'

        return timeAndDistanceMatrix

    def sendRequest(self, location, origin):
        # print("new send_request")  # TODO - remove debug once verified
        """ Build and send request for the locations"""

        request = self.configuration.getMapURL()
        body = {
            'locations': location,
            'metrics': self.configuration.getMetrics().split(','),
            'units': self.configuration.getUnits(),
            'sources': origin
        }
        API_key = self.RReq.getOptimizationAPIKey()
        try:
            headers = {
                'Authorization': API_key,
            }
            response = requests.post(request, json=body, headers=headers)
            if response.status_code == 200:
                return json.loads(response.text)
            else:
                print("Error code ", response.status_code)
        except requests.exceptions.RequestException as e:
            print("Given error...", e)
        # current_delay = 0.1  # Set the initial retry delay to 100ms.
        # max_delay = 50  # Set the maximum retry delay to 5 seconds.
        # while True:
        #     try:
        #         headers = {
        #             "Authorization": API_key,
        #         }
        #         response = requests.post(request, json=body, headers=headers)
        #     except requests.exceptions.RequestException as e:
        #         print("Given error...", e)
        #
        #     else:
        #         if response.status_code == 200:
        #             return json.loads(response.text)
        #         # else:
        #         #     if 399 < response.status_code < 420:
        #         #         print("\nORS probably rate limiting. Will try with different API key\n")
        #         #         API_key = random.choice(API_KEYS_LIST)
        #         #     print("Error....", response)
        #     if current_delay > max_delay:
        #         raise Exception("Too many retry attempts.")
        #     print("Waiting", current_delay, "seconds before retrying.")
        #     time.sleep(current_delay)  # current_delay *= 1.5  # Increase the delay each time we retry.
        # header = {
        #     'Authorization': self.RReq.getOptimizationAPIKey()
        # }
        # body = {
        #     'locations': long_lat,
        #     'metrics': self.configuration.getMetrics().split(','),
        #     'units': self.configuration.getUnits()
        # }
        # map_api = self.configuration.getMapURL()
        # self.logger.info(body)
        # DistanceMatrix = {'data': [], 'status': "SUCCESS"}
        # try:
        #     response = requests.post(url=map_api, headers=header, json=body)
        #
        # except requests.exceptions.RequestException:
        #     self.logger.error('No response from OpenRouteMapAPI.')
        #     DistanceMatrix['status'] = "FAILURE"
        #     return DistanceMatrix
        #
        # if response.status_code != 200:
        #     header['Authorization'] = self.configuration.getApikey()
        #     response = requests.post(url=map_api, headers=header, json=body)
        #     if response.json().get('error') is not None:
        #         self.logger.error(response.json()['error'])
        #         DistanceMatrix['status'] = "INVALID"
        #         return DistanceMatrix
        #     if response.status_code != 200:
        #         self.logger.error('No response from OpenRouteMapAPI.')
        #         DistanceMatrix['status'] = "FAILURE"
        #         return DistanceMatrix
