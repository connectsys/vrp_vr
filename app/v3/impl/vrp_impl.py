import os
from functools import partial
import json
from ...config.vrpconfig import VRPConfig
from ..entity.route_response import Trip, TripDetail, PydanticRouteResponse, ErrorClass, Order
from ..entity.route_request import RouteRequest
from .open_route_map_impl import OpenRouteImpl
import statistics
from time import perf_counter
from ..entity.vroom_request import VroomRequest, VroomDurationCost, VroomShipment, VroomVehicle, VroomVehicleStep, VroomShipmentStep, VroomJob, VroomBreak, VroomMatrices
import subprocess
import pydantic
from ..entity.vroom_response import VroomResponse


class VRPImpl:
    def __init__(self, routeRequest):
        self.configurations = VRPConfig()
        assert isinstance(routeRequest, RouteRequest)
        self.RIReq = routeRequest
        self.url = self.configurations.getMapURL()
        self.ORM = OpenRouteImpl(routeRequest)

    def getRoutes(self):
        distance_matrix = self.ORM.getRoutes()
        print("Distance Matrix received")
        if type(distance_matrix) == list:
            routeInfoResponse = self.calculateRoutesVroom(
                distance_matrix[0], distance_matrix[1])
            # routeInfoResponse = self.calculateRoutes(distance_matrix[0])
            return routeInfoResponse
        return distance_matrix

    def getCoeff(self, distanceMatrix, timeMatrix):
        dis_l = []
        time_l = []

        lis_coeff = []
        for r1 in distanceMatrix:
            for r in r1:
                dis_l.append(r)
        for r1 in timeMatrix:
            for r in r1:
                time_l.append(r)

        i = 0
        while i < len(dis_l):
            if time_l[i] != 0 and dis_l[i] != 0:
                lis_coeff.append(dis_l[i] / time_l[i])
            i += 1
        coeff = statistics.median(lis_coeff)
        # coeff = (max(set(lis_coeff), key = lis_coeff.count))

        # print("coefficient is ",coeff)
        return coeff

    def calculateRoutesVroom(self, distanceMatrix, timeMatrix):
        RReq = self.RIReq
        ordersList = RReq.getOrders()
        locationList = RReq.getLocationDetails()
        driver_shifts = RReq.getDriverShifts()
        vehiclesList = RReq.getVehicleDetails()
        jobs = []
        shipments = []
        for order in ordersList:
            pickIndex, deliverIndex = order.pickupIndex, order.deliveryIndex
            if pickIndex and deliverIndex is None:
                service = order.serviceTimePickup
                time_windows = None
                if order.pickupTimeWindow and order.pickupTimeWindow.startTime and order.pickupTimeWindow.endTime:
                    time_windows = [order.pickupTimeWindow.startTime, order.pickupTimeWindow.endTime]

                pickup = [order.weight, order.volume]
                if time_windows:
                    job = VroomJob(id=order.orderId, location_index=pickIndex, service=service,
                                   pickup=pickup, time_windows=[time_windows])
                else:
                    job = VroomJob(id=order.orderId, location_index=pickIndex, service=service,
                                   pickup=pickup)
                jobs.append(job)
            if deliverIndex and pickIndex is None:
                service = order.serviceTimeDelivery
                time_windows = None
                delivery = [order.weight, order.volume]
                if order.deliveryTimeWindow and order.deliveryTimeWindow.startTime and order.deliveryTimeWindow.endTime:
                    time_windows = [order.deliveryTimeWindow.startTime, order.deliveryTimeWindow.endTime]
                if time_windows:
                    job = VroomJob(id=order.orderId, location_index=deliverIndex, service=service,
                                   delivery=delivery, time_windows=[time_windows])
                else:
                    job = VroomJob(id=order.orderId, location_index=deliverIndex, service=service,
                                   delivery=delivery)
                jobs.append(job)
            if pickIndex and deliverIndex:
                time_windows = None
                if order.pickupTimeWindow and order.pickupTimeWindow.startTime and order.pickupTimeWindow.endTime:
                    time_windows = [order.pickupTimeWindow.startTime, order.pickupTimeWindow.endTime]
                if time_windows:
                    pickup = VroomShipmentStep(id=order.orderId, location_index=pickIndex,
                                               service=order.serviceTimePickup, time_windows=[time_windows])
                else:
                    pickup = VroomShipmentStep(id=order.orderId, location_index=pickIndex,
                                               service=order.serviceTimePickup)
                time_windows = None
                if order.deliveryTimeWindow:
                    if order.deliveryTimeWindow.startTime and order.deliveryTimeWindow.endTime:
                        time_windows = [order.deliveryTimeWindow.startTime, order.deliveryTimeWindow.endTime]
                if time_windows:
                    delivery = VroomShipmentStep(id=order.orderId, location_index=deliverIndex,
                                                 service=order.serviceTimeDelivery, time_windows=[time_windows])
                else:
                    delivery = VroomShipmentStep(id=order.orderId, location_index=deliverIndex,
                                                 service=order.serviceTimeDelivery)
                amount = [order.weight, order.volume]
                shipment = VroomShipment(pickup=pickup, delivery=delivery, amount=amount)
                shipments.append(shipment)
        vehicles = []
        count = 0
        vehIdDict = {}
        for veh in vehiclesList:
            vehIdDict[veh.shiftId+veh.vehicleId] = veh
            if driver_shifts and driver_shifts[count]:
                vehicle = VroomVehicle(id=veh.shiftId+veh.vehicleId, start_index=0, end_index=0,
                                       capacity=[veh.weightCapacity, veh.volumeCapacity],
                                       time_windows=[driver_shifts[count]],
                                       max_tasks=veh.orderCapacity+2)
            else:
                vehicle = VroomVehicle(id=veh.shiftId + veh.vehicleId, start_index=0, end_index=0,
                                       capacity=[veh.weightCapacity, veh.volumeCapacity],
                                       max_tasks=veh.orderCapacity+2)
            count += 1
            vehicles.append(vehicle)

        vroom_input = VroomRequest(jobs=jobs, shipments=shipments, vehicles=vehicles,
                                   matrices=VroomMatrices(car=VroomDurationCost
                                   (durations=timeMatrix, costs=distanceMatrix)))
        with open("vroom_ip.json", "w") as outfile:
            outfile.write(vroom_input.json(exclude_none=True, exclude_defaults=True,
                                           exclude_unset=True))
        os.system('vroom -i vroom_ip.json -o vroom_op.json')
        return VroomResponse(**json.load(open('vroom_op.json'))).ConvRRes(vehIdDict, locationList)
