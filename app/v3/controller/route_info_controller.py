import logging
from ..impl.vrp_impl import VRPImpl
from ..entity.route_response import ErrorClass


class RouteController:
    def __init__(self):
        self.logger = logging.getLogger('route_info_controller')

    def getRoutes(self, routeRequest):
        try:
            if routeRequest.errorMessages:
                return ErrorClass(errorMessages=routeRequest.errorMessages)
            route_info_response = VRPImpl(routeRequest).getRoutes()
            return route_info_response
        except Exception as e:
            self.logger.error('', exc_info=True)
            return ErrorClass(errorMessages=['Some Unknown Error Occured.'])
