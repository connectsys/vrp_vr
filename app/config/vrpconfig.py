import logging
import os


class VRPConfig:

    logging.info("Configuring VRP")

    def __init__(self, geoCode = 'openRouteMapURL', units = 'm'):
        self._apikey = os.getenv('openRouteMapAPI')
        avail_geoCode = os.getenv('availGeoCode').split(',')
        if geoCode and geoCode in avail_geoCode:
            self._mapDistanceServiceURL = os.getenv(geoCode)
        else:
            self._mapDistanceServiceURL = os.getenv('openRouteMapUrl')
        self._metrics = os.getenv('metrics')
        avail_units = os.getenv('availUnits').split(',')
        if units and units in avail_units:
            self._units = units
        else:
            self._units = os.getenv('units')
        self._timeLimits = int(os.getenv('timeLimit'))
        self.minTimeLimit = int(os.getenv('minTimeLimit'))
        self.maxTimeLimit = int(os.getenv('maxTimeLimit'))
        self.timeLimitFactor = float(os.getenv('timeLimitFactor'))

    def getApikey(self) -> str:
        return self._apikey

    def getMapURL(self) -> str:
        return self._mapDistanceServiceURL

    def getMetrics(self) -> str:
        return self._metrics

    def getUnits(self) -> str:
        return self._units

    def getTimeLimit(self) -> int:
        return self._timeLimits
    
    def getMinTimeLimit(self) -> int:
        return self.minTimeLimit 
    
    def getMaxTimeLimit(self) -> int:
        return self.maxTimeLimit
    
    def getTimeLimitFactor(self):
        return self.timeLimitFactor
