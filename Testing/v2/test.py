import sys,json,unittest,dotenv
sys.path.append('../..')
from fastapi.testclient import TestClient
from app.api import app
dotenv.load_dotenv(dotenv_path='../../.env')
class TestVrsAI(unittest.TestCase):
    maxDiff=None
    route='/get-routes'
    @classmethod
    def setUpClass(cls):
        cls.client=TestClient(app)
        with open('TestData_V2.json') as f:
            cls.testData=json.load(f)
        return cls
    def test_PickDrop_requests(self):
        print('PickDrop TestCases Started.')
        for Transaction in self.testData['Transactions']['SingleVehicle']['Pickup&DeliveryRequests']:
            response=self.client.post(self.route,json=Transaction['RequestJson'])
            self.assertEqual(response.status_code,Transaction['StatusCode'])
            self.assertEqual(response.json(),Transaction['ResponseJson'])
    def test_tsp_requests(self):
        print('TSP testCases Started.')
        for Transaction in self.testData['Transactions']['SingleVehicle']['TSPRequests']:
            response=self.client.post(self.route,json=Transaction['RequestJson'])
            self.assertEqual(response.status_code,Transaction['StatusCode'])
            self.assertEqual(response.json(),Transaction['ResponseJson'])
    def test_bad_requests(self):
        print('Bad TestCases Started.')
        for Transaction in self.testData['Transactions']['SingleVehicle']['Bad_Requests']:
            response=self.client.post(self.route,json=Transaction['RequestJson'])
            self.assertEqual(response.status_code,Transaction['StatusCode'])
            self.assertEqual(response.json(),Transaction['ResponseJson'])
        print('Bad TestCases Ended.')
if __name__=='__main__':
    unittest.main()