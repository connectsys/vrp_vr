import sys,json,unittest,dotenv
sys.path.append('../..')
from fastapi.testclient import TestClient
from app.api import app
dotenv.load_dotenv(dotenv_path='../../.env')
class TestVrsAI(unittest.TestCase):
	maxDiff=None
	route='/route-optmization'
	@classmethod
	def setUpClass(cls):
		cls.client=TestClient(app)
		with open('TestData.json') as f:
			cls.testData=json.load(f)
		return cls
	def generalTest(self,testData):
		for Transaction in testData:
			response=self.client.post(self.route,json=Transaction['RequestJson'])
			self.assertEqual(response.status_code,Transaction['StatusCode'])
			totalDistance=sum([i['distanceTravelled'] for i in response.json()['tripOrders']])
			self.assertLessEqual(totalDistance/1000,Transaction['Response'])
	def testTspRequests(self):
		self.generalTest(self.testData['Transactions']['SingleVehicle']['TSP'])
	def testPandDRequests(self):
		self.generalTest(self.testData['Transactions']['SingleVehicle']['P&D'])
	def testPandDWithItems(self):
		self.generalTest(self.testData['Transactions']['SingleVehicle']['P&D With Items'])
	def testMultiVehiclePandDWithItems(self):
		self.generalTest(self.testData['Transactions']['MultipleVehicle']['P&D With Items'])
	def testMultipleTspRequests(self):
		self.generalTest(self.testData['Transactions']['MultipleVehicle']['TSP'])
	def testMultiplePandDRequests(self):
		self.generalTest(self.testData['Transactions']['MultipleVehicle']['P&D'])
	def testBadRequests(self):
		for index,Transaction in enumerate(self.testData['Transactions']['BadRequests']):
			print(index)
			response=self.client.post(self.route,json=Transaction['RequestJson'])
			self.assertEqual(response.status_code,Transaction['StatusCode'])
			self.assertEqual(response.json(),Transaction['Response'])
if __name__=='__main__':
	unittest.main()