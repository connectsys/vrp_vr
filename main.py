import uvicorn
import logging
from app.api import app

logging.basicConfig(format='%(name)s, %(asctime)s, %(message)s', datefmt='%d/%m/%Y %I:%M:%S %p',
                    level=logging.WARNING, force=True)
logger = logging.getLogger('main.py')
logger.info("VRP Application starting Vroom.")
if __name__ == "__main__":
    uvicorn.run(app, host='127.0.0.1', port=8000, log_level="warning")
