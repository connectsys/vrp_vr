from distutils.core import setup
from distutils.extension import Extension
from Cython.Distutils import build_ext

ext_modules = [Extension("cloneNodes", ["app/v3/impl/cloneNodes.pyx"])]

setup(
    name = 'arr',
    cmdclass = {'build_ext': build_ext},
    ext_modules = ext_modules,
    compiler_directives={'language_level' : "3"}
)